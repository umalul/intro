<?php
    include "class.php"; // שימוש בקובץ class.php
    include "db.php";
    include "query.php";
?>
<html>
    <head>
    <title>Object Oriented PHP Class1</title>
    </head>
    <body>
        <p>
        <?php
        /* $text = 'Hello World';
            echo "$text And The Universe ";
            echo '<br>';
            $msg = new Message(); // יצירת אובייקט מסוג Message
            // echo $msg->text; // את האובייקט הכנסנו למשתנה והדפסנו
            $msg->show();
            // echo Message::$count; 
            $msg1 = new Message("A new text"); // שינינו את הטקסט
            $msg1->show();
            // echo Message::$count; 
            $msg2 = new Message(); // כתיבת טקסט דיפולטיבי בגלל שריק
            $msg2->show();
            echo '<br>';
            echo Message::$count; // הצגת ערך
            echo '<br>';
            $msg3 = new redMessage('A red message');
            $msg3->show();
            echo '<br>';
            $msg4 = new coloredMessage('A colored message');
            $msg4->color = 'green';
            $msg4->show();
            //$msg4->setColor('red'); שיטה מורבלת ליצירת פונקציה שתקבל את שם הצבע
            echo '<br>';
            $msg4->color = 'yellow';
            $msg4->show();
            echo '<br>';
            $msg4->color = 'green';
            $msg4->show();
            echo '<br>';
            $msg4->color = 'black';
            $msg4->show();
            echo '<br>';
            $msg5 = new coloredMessage('A colored message');
            $msg5->color = 'black';
            $msg5->show();
            $msg5->color = 'green';
            $msg5->color = 'black';
            echo '<br>';
            $msg5->show();
            echo '<br>';
            showObject($msg5);
            echo '<br>';
            showObject($msg1);
            /*var_dump($msg);
            $msg->show();*/
            # שיעור DB
            $db = new DB('localhost','intro','root','');
            $dbc = $db->connect();
            $query = new Query($dbc);
            $q = "SELECT * FROM users";
            $result = $query->query($q);
            echo '<br>';
           # echo $result->num_rows זו שורת בדיקה
           if($result->num_rows > 0){
               echo '<table>';
               echo '<tr><th>Name</th><th>Email</th></tr>';
               while($row = $result->fetch_assoc()){#fetch_assoc - לוקחת רשומה ראשונה מהטבלה שיש בזכרון ובונה מערך אסוציאטיבי ולאחר מכן מוחקת את הרשומה הזו
                echo '<tr>';
                echo '<td>'.$row['name'].'</td><td>'.$row['email'].'</td>';
                echo '</tr>';
               }
               echo '</table>';
           }else{
               echo "Sorry no results";
           }

            
        ?>
        </p>
    </body>
</html>