<?php
    class Message {
        // option 1
       //  public $text = 'A simple message'; //תכונה שערכה מה שכתבנו
       // option 2
       protected $text = 'A simple message';
       public static $count = 0; // תכונה ששייכת למחלקה עצמה
       public function show(){ //שיטה
           echo "<p> $this->text </p>";
       }
       function __construct($text = ""){ // מתן ערך השמה דיפולטיבי
            ++self::$count; // בכל יצירת אובייקט מסוג הודעה (מסג') הספירה תעלה ב1
            if($text != ""){
                $this->text = $text;
            }
        }
    }
    class redMessage extends Message {
        public function show(){
            echo "<p style = 'color: red'> $this->text</p>";
        }
    }
    class coloredMessage extends Message {
        protected $color = 'red';
        public function __set($property,$value){//property-שם התכונה value-ערך שנרצה להכניס
            if($property == 'color'){
                $color = array('red','yellow','green');
                if(in_array($value,$color)){
                    $this->color = $value;
                }
                else{
                    echo '<script>alert("Please select a color that available in the system")</script>';
                    }
                }
            }
        
        public function show(){
            echo "<p style = 'color: $this->color'> $this->text</p>";
        }
    }
    function showObject($object){
        $object->show();
    }
//comment 1
#bla bla
#uri

?>